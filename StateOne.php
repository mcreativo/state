<?php


class StateOne implements StateInterface
{

    public function methodA()
    {
        echo 'methodA doing something related with the state one' . PHP_EOL;
    }

    public function methodB()
    {
        echo 'methodB doing something related with the state one' . PHP_EOL;
    }

} 