<?php


class StateTwo
{

    public function methodA()
    {
        echo 'methodA doing something related with the state two' . PHP_EOL;
    }

    public function methodB()
    {
        echo 'methodB doing something related with the state two' . PHP_EOL;
    }

} 