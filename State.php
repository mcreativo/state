<?php


class State implements StateInterface
{

    /**
     * @var StateOne
     */
    protected $stateOne;

    /**
     * @var StateTwo
     */
    protected $stateTwo;

    /**
     * @var State
     */
    protected $currentState;

    public function methodA()
    {
        $this->currentState->methodA();
    }

    public function methodB()
    {
        $this->currentState->methodB();
    }

    public function createStateOne()
    {
        return new StateOne();
    }

    public function createStateTwo()
    {
        return new StateTwo();
    }


    public function setStateOne()
    {
        if ($this->stateOne == null)
            $this->stateOne = $this->createStateOne();
        $this->currentState = $this->stateOne;
    }

    public function setStateTwo()
    {
        if ($this->stateTwo == null)
            $this->stateTwo = $this->createStateTwo();
        $this->currentState = $this->stateTwo;
    }

} 